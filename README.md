- [Project title: Console FinOps MultiCloud](#project-title-console-finops-multicloud)
  - [Description of the context](#description-of-the-context)
  - [Project objectives](#project-objectives)
  - [Expected work](#expected-work)
  - [Techniques, tools, technologies, languages and frameworks to implement](#techniques-tools-technologies-languages-and-frameworks-to-implement)

# Project title: Console FinOps MultiCloud

Polytech Grenoble INFO5 Project Proposal S10 2023-2024

Leaders: [Chems-Eddine Hadiby](https://www.linkedin.com/in/chemss-eddine-hadiby) - [mail](mailto:chemsseddine.hadiby@gmail.com) , Didier Donsez

The aim of the project is to develop a [FinOps](https://air.imag.fr/index.php/FinOps) console for a user/organization (project manager, product owner, CFO) using several Cloud solutions (public or private).

## Description of the context

Faced with the rise of the native cloud, more and more companies are opting for multi-cloud solutions/strategies. A multicloud strategy is the use of at least two Cloud Computing services from any number of Cloud providers. This enables companies to avoid vendor lock-in. They see it as a way of increasing their flexibility, resilience and budget efficiency. However, this multiplicity of platforms complicates the measurement of financial indicators, especially for non-technical profiles such as project managers, product owners or CFOs.

These stakeholders need to understand the use of resources to make informed decisions on investments and costs. However, governance of roles and access in a multi-cloud environment can be complex and difficult to manage.

This can lead to security, compliance and operational efficiency issues.

Consequently, robust tools and processes need to be put in place to effectively manage multi-cloud environments, while enabling clear and comprehensible visualization of financial metrics for all stakeholders. This is the challenge that this project aims to address.

## Project objectives

The aim of this project is to design a FinOps console for a user or organization (project manager, product owner, CFO) using various Cloud solutions, whether public or private.

This FinOps console is based on a temporal database that records the actual consumption of resources used by several cloud providers, such as AWS, GCP, Azure, OVH, etc. These consumptions are retrieved via a graphical user interface. These consumptions are retrieved via the cloud providers' REST APIs.

The FinOps console will provide users with cost forecasts (monthly/annual) with aggregations at project, departmental and organizational level.

## Expected work

The expected work will consist in developing a minimum viable product (MVP) that will include the following elements:
- Exploitation of at least 2 cloud providers, such as Azure, in line with your previous experience during the Ecom project. Other candidates include Google CP, AWS and France's OVH.
- A REST API to retrieve FinOps data and metrics.
- A BI (Business Intelligence) interface of your choice, for example, a generated JHipster, Grafana, Tableau, PowerBI... etc.
- The service must be multi-tenant (a tenant is a company/business unit of a company) and multi-project (several finops projects per tenant). Users within a tenant are either administrators or viewers (who cannot modify or add to a finops project).
- 
Particular importance should be attached to documenting your work, including code documentation, explanations of design choices, architecture diagrams for your mechanism, installation and user instructions, and any other information that might make it easier for other teams to understand and extend your project.

Beyond the basic requirements, there are many ways to improve your project. Consider the following enhancements:

Integrate an additional cloud provider into the API.
Develop an ergonomic user interface, suitable for non-IT users.
Automate the project's infrastructure (Infrastructure as Code) using Terraform.
Enhance the security of the data retrieval process, for example by securing the API or using the Workload Identity Federation or a similar mechanism.
These enhancements are not mandatory, but they will be considered a bonus for your work.

## Techniques, tools, technologies, languages and frameworks to implement
- You can choose from several cloud platforms, such as AWS, GCP, Azure, OVH, Oracle, CloudFlare, Digital Ocean, Heroku, AliBaba, etc.
- For the console (front end), you can use JHipster, Grafana or Tableau (using the UGA license), or any other tool of your choice.
- For APIs, the choice of language and technology is free. However, the API will have to be documented using the OpenAPI v3 formalism (which is already used to expose the REST API of backends generated with JHipster). It's best to opt for scalable, viable and well-documented tools, such as FastApi, Flask, Express.js, Spring Boot, Django, Ruby on Rails, etc.

![Architecture proposal](./assets/images/architecture_service_finops.jpg)