# Project design

## Context diagram

![Context diagram](../assets/images/context_diagram.png)


## List of potential functionalities

- retrieve FinOps data and metrics
- authenticate and connect to system
- store metrics in database
- update metrics in database
- display BI interface
- multi-modality support (web, mobile, desktop)
- add an extension for a new cloud service
- add/modify a company
- add/edit a project
- add/edit a user (project manager / owner / CFO)
- configure/modify access to project FinOps metrics
- configure/modify BI interface
- display wiki documentation (API)
- display price lists for various cloud services
- encrypt traffic

## High-level logical view 

![High-level logical view](../images/high-level-logical-view.png)

## Logical detail view

![Detailed front-end logical view](../images/frontend-detailled-logical-view.png)

![Detailed back-end logical view](../images/backend-detailled-logical-view.png)

## Class diagram

![Class diagram](../images/class-diagram.png)