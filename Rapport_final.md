# Rapport final

<!-- TOC start  -->

- [Rappel du sujet/besoin et cahier des charges](#rappel-du-sujetbesoin-et-cahier-des-charges)
- [Les technologies utilisés](#les-technologies-utilisés)
- [Architecture technique](#architecture-technique)
- [Réalisations techniques](#réalisations-techniques)
- [Gestion de projet](#gestion-de-projet)
- [Métriques logiciels](#métriques-logiciels)
- [Conclusion](#conclusion)

<!-- TOC -->


## Rappel du sujet/besoin et cahier des charges
Suite à l'émergence des services clouds natif , les entreprises utilisent parfois différents services cloud pour optimiser leur dépenses ou leur flexibilité.
Cependant, l’utilisation de plusieurs solutions cloud au sein d’un projet, rend parfois difficile le suivi des dépenses à l’utilisation des services pour les directeurs financiers.
Afin d’adapter la stratégie ou simplement de suivre les dépenses facilement pour différents services cloud associés à un même projet, nous proposons de développer un outil permettant de stocker, combiner et visualiser ces dépenses.

L’objectif de ce projet : 
- récupérer les métriques de coût des différents services clouds utilisés pour différents projets grâce aux API mises à disposition par les fournisseurs cloud.
- stocker ces métriques dans une base de données.
- rendre les données accessibles par les différents membres d’un projet en gérant les contrôles d’accès.
- permettre de visualiser ces données efficacement grâce à des interface BI comme Grafana ou powerBI.
- mettre en place de la gestion d’accès pour les utilisateurs (projet et entreprise) afin de maîtriser l’accès aux données par les utilisateurs.

## Les technologies utilisés
- GitLab a été choisi pour notre projet en raison de sa flexibilité accrue pour le DevOps, de sa gestion de projet améliorée et de ses puissantes fonctionnalités de CI/CD. De plus, l'intégration native de GitLab avec Docker nous permet de versionner directement les conteneurs Docker.

- NestJS a été choisi pour son architecture modulaire et orientée objet, offrant une base solide pour le développement backend. Cette décision répond au souhait d'apprentissage de certains membres de l'équipe intéressés par Node.js. Son intégration transparente avec Express.js et sa facilité de maintien ont renforcé notre choix.

- Grafana a été préféré à PowerBI, étant donné que ce dernier est uniquement disponible sur Windows. Grafana se distingue particulièrement dans la visualisation de données temporelles, mais reste polyvalent grâce à sa compatibilité avec divers plugins, permettant ainsi l'observation de données variées.

- Nous avons choisi PostgreSQL pour notre base de données en raison de sa familiarité pour l'équipe, de sa nature open source et de sa fiabilité éprouvée. 

- Prisma a été choisi comme ORM pour notre projet en raison de ses nombreux avantages. Conçu avec TypeScript à l'esprit, il s'intègre parfaitement dans notre stack technologique, simplifiant ainsi le développement et la maintenance du code. Son approche déclarative réduit la complexité du code et améliore sa lisibilité, ce qui en fait un choix optimal pour notre projet.

- Nous avons choisi d'intégrer SonarQube dès le début de notre projet, tirant parti de notre expérience précédente sur le projet ECOM. SonarQube nous permet d'appliquer les meilleures pratiques en matière d'analyse statique du code, garantissant ainsi la qualité et la sécurité du code tout au long du processus de développement.

- Docker a été intégré dès le début de notre projet, s'appuyant sur son efficacité éprouvée dans nos projets antérieurs. Son utilisation assure la cohérence et la portabilité des applications, simplifiant ainsi le déploiement et la maintenance tout au long du cycle de vie du projet.

## Architecture technique
![Detailed front-end logical view](assets/images/frontend-detailled-logical-view.png)

![Detailed back-end logical view](assets/images/backend-detailled-logical-view.png)


## Réalisations techniques

### Récupérations de métriques de coût pour tous les services cloud (AWS, Azure, Heroku) 
Création de fonction schedulée faisant appel à l’API des différents services cloud pour récupérer et stocker dans la base de données les métriques
Nous avons créer des fonctions programmées faisant appel aux API des différents services cloud permettant de récupérer les métriques de coûts de ces derniers.

### CRUDs 
Pour chaques services clouds et tables de la base de données nous avons mis en place des fonctions permettant de les manipulées:
- Create : insertions de nouvelles metriques, identifiants (pour accéder aux services clouds), entreprises, projet et utilisateurs
- Retrieve : récupérations de chaques instances dans la base de données pour chaque table
- Update: mise à jour des informations pour les entreprises, projets, utilisateurs et identifiants
- Delete : Suppression des instances des entreprises, projets, utilisateurs et identifiants.

### Role based access control 
Afin de sécuriser l'API, nous avons mis des contrôles d'accès à base de rôles permettant seulement aux personnes autorisés à consulter ou modifier certaines ressources

### Agrégation des coûts stockés dans la base de données
Afin de faire remonter les couts de tous les services clouds utilisés pour un projet, nous avons créer des fonctions aggrégeant ces couts et les faisant remonter aux utilisateurs

### Documentation Swagger
Pour mieux comprendre les différents endpoints de notre API, nous avons mis en place une documentation grâce à l'API Swagger 

### Prototype d’interface BI (à améliorer)
Pour avoir une prototype de visualisation de l'interface BI, nous avons fait créer une interface BI qui pourra être améliorer par la suite à l'aide d'une personne plus compétente en finance qui pourra suggérer de meilleurs visualisations et informations à faire remonter pour l'utilisateur.


## Gestion de projet

Pour assurer une collaboration efficace et une gestion optimale du projet, nous avons adopté une approche agile, structurée en plusieurs étapes.

### Organisation collaborative

Travaillant sans rôles définis, tous les membres de l'équipe ont contribué de manière égale, travaillant à la même échelle, sauf pour la partie DevOps qui a été prise en charge par Lilian. En tant que trinôme, nous avons privilégié la coopération et la répartition des tâches pour optimiser notre productivité. Cette collaboration s'est appuyée sur deux dépôts GitLab distincts pour gérer les versions du back-end de notre application et de la documentation.

### Gestion des tâches et communication

Lorsque nous identifions des améliorations ou des fonctionnalités à ajouter, nous ouvrons une Issue sur GitLab, précisant la problématique et l'objectif visé. Nous utilisons des "tags" pour catégoriser les problématiques en fonction de la méthode MoSCoW, de leur domaine (sécurité, optimisation, etc.) et de leur état (backlog, planned, doing, done, reviewed). Ces tags permettent une vision claire des tâches à réaliser et sont organisés dans un tableau Kanban pour le suivi visuel.

![Liste d'issues](assets/images/issues-list.png)

![Tableau KANBAN](assets/images/kanban-board.png)

### Planification hebdomadaire

Au début de chaque semaine, nous planifions les tâches prioritaires à accomplir dans la semaine. Nous effectuons une estimation du temps nécessaire pour chaque tâche et répartissons le travail en conséquence. Nous organisons également des réunions quotidiennes pour assurer une communication transparente et un suivi régulier des progrès.

### Revue Client

Nous avons des échanges réguliers avec le porteur de projet pour présenter le travail effectué et discuter des orientations futures. Cela nous permet de maintenir une communication étroite et de nous assurer que notre travail est aligné avec les attentes et les objectifs du projet.

### Gestion du code

Lorsque nous commençons à travailler sur une issue, nous créons une branche spécifique correspondant à cette issue sur le dépôt GitLab. Nous travaillons sur cette branche pour réaliser les objectifs fixés. Une fois la tâche achevée, nous soumettons une MR et demandons une validation de l'autre personne. Cette approche de code review permet de détecter les erreurs et d'optimiser le code. Après avoir corrigé les retours, la branche issue est fusionnée dans la branche develop.

![Liste de merge request](assets/images/merge-requests-list.png)


### Intégration et déploiement continue

Pour garantir la qualité et la fiabilité du code, nous avons mis en place des étapes d'intégration continue, y compris des tests, des builds et des vérifications de conformité. Nous utilisons également des conteneurs Docker pour assurer la portabilité et la répétabilité du déploiement.

![Pipeline](assets/images/pipeline.png)


### Différents environnements de développement

Un script automatisé sur GitLab déclenche le déploiement de l’api dès que des modifications sont apportées aux branches main et develop. Cela nous permet d'avoir un environnement de test pour valider les fonctionnalités avant de les déployer dans un environnement de production.

En résumé, notre gestion de projet repose sur des méthodes agiles, des outils de versionnement, des processus de validation rigoureux et une collaboration étroite avec le tuteur et les collègues. Cela nous permet de maintenir une communication fluide, de garantir la qualité du code et de répondre aux besoins changeants du projet tout en respectant les délais.

### Gestion des risques

![Analyse des risques](assets/images/risk-analysis.png)


## Métriques logiciels

![Ligne de code](assets/images/lines-of-code.png)

![Répartition des commits](assets/images/distribution-of-commits.png)

Le nombre de commits dans un projet de développement logiciel ne donne pas toujours une image précise de l'effort et du travail fourni par les développeurs. Des tâches nécessitant beaucoup de temps, telles que la résolution de problèmes de performance ou de sécurité, peuvent se traduire par peu de lignes de code modifiées. De même, des activités telles que la maintenance ou la documentation peuvent exiger un investissement important en temps mais entraîner peu de modifications dans le dépôt. Ainsi, il est essentiel de ne pas se fier uniquement au nombre de commits pour évaluer la contribution d'un membre de l'équipe de développement.

### Conclusion

En conclusion, notre projet de développement d'une console FinOps MultiCloud a été une expérience enrichissante malgré les défis rencontrés. Malheureusement, en raison d'un manque de temps et de ressources humaines, le projet n'a pas été achevé dans son intégralité. Bien que nous ayons réussi à mettre en place un backend fonctionnel avec des routes dans notre API afin d'avoir accès à toutes les métriques souhaitées pour chaque fournisseur cloud, l'affichage de toutes ces métriques combinéees dans Grafana n'a pas pu être finalisé dans les temps.

Néanmoins, nous avons réalisé des progrès significatifs dans la conception et le développement de cette solution. En utilisant des technologies modernes et en adoptant des méthodes agiles, nous avons construit une base solide sur laquelle les futurs étudiants info qui reprendront le projet pourront continuer à travailler.


