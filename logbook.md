# logbook

<!-- TOC -->
- [logbook](#logbook)
  - [__Week 1 29th of January - 2nd of February__ :](#week-1-29th-of-january---2nd-of-february-)
    - [__Monday 29th of January__ :](#monday-29th-of-january-)
    - [__Tuesday 30th of January__ :](#tuesday-30th-of-january-)
    - [__Wednesday 31th of January__ :](#wednesday-31th-of-january-)
    - [__Thursday 1st of February__ :](#thursday-1st-of-february-)
    - [__Friday 2nd of February__ :](#friday-2nd-of-february-)
  - [__Week 2 5th of February - 9th of February__ :](#week-2-5th-of-february---9th-of-february-)
    - [__Monday 5th of February__ :](#monday-5th-of-february-)
    - [__Tuesday 6th of February__ :](#tuesday-6th-of-february-)
    - [__Wednesday 7th of February__ :](#wednesday-7th-of-february-)
    - [__Thursday 8th of February__ :](#thursday-8th-of-february-)
    - [__Friday 9th of February__ :](#friday-9th-of-february-)
  - [__Week 3 12th of February - 16th of February__ :](#week-3-12th-of-february---16th-of-february-)
    - [__Monday 12th of February__ :](#monday-12th-of-february-)
    - [__Tuesday 13th of February__ :](#tuesday-13th-of-february-)
    - [__Wednesday 14th of February__ :](#wednesday-14th-of-february-)
    - [__Thursday 15th of February__ :](#thursday-15th-of-february-)
    - [__Friday 16th of February__ :\*](#friday-16th-of-february-)
  - [__Week 4 19th of February - 23th of February__ :](#week-4-19th-of-february---23th-of-february-)
    - [__Monday 19th of February__ :](#monday-19th-of-february-)
    - [__Tuesday 20th of February__ :](#tuesday-20th-of-february-)
    - [__Wednesday 21th of February__ :](#wednesday-21th-of-february-)
    - [__Thursday 22th of February__ :](#thursday-22th-of-february-)
    - [__Friday 23th of February__ :](#friday-23th-of-february-)
  - [__Week 5 26th of February - 30th of February__ :](#week-5-26th-of-february---30th-of-february-)
    - [__Monday 26th of February__ :](#monday-26th-of-february-)
  - [__Week 6 5th of March - 9th of March__ :](#week-6-5th-of-march---9th-of-march-)
    - [__Monday 5th of March__ :](#monday-5th-of-march-)
    - [__Tuesday 6th of March__ :](#tuesday-6th-of-march-)
    - [__Wednesday 7th of March__ :](#wednesday-7th-of-march-)
    - [__Thursday 8th of March__ :](#thursday-8th-of-march-)
    - [__Friday 9th of March__ :](#friday-9th-of-march-)
  - [__Week 7 9th of March - 14th of March__ :](#week-7-9th-of-march---14th-of-march-)
    - [__Friday 9th of March__ :](#friday-9th-of-march--1)
    - [__Monday 12th of March__ :](#monday-12th-of-march-)
    - [__Tuesday 13th of March__ :](#tuesday-13th-of-march-)
    - [__Wednesday 14th of March__ :](#wednesday-14th-of-march-)
<!-- TOC -->

## __Week 1 29th of January - 2nd of February__ :

### __Monday 29th of January__ : 
- Meeting with Chems to give us explanations about the project
- Account creation on the different cloud services

### __Tuesday 30th of January__ : 
- Conception : [conception diagrams](./assets/markdown/conception.md) - Lilian & Yann
- Planification - Lilian & Victor & Yann

### __Wednesday 31th of January__ :
- Add changelog to the project - Lilian
- Learning of the technogies - Yann & Victor

### __Thursday 1st of February__ :
- Work on adding pipeline to the project - Lilian
- Learning of the technogies - Yann & Victor

### __Friday 2nd of February__ :
- Add pipeline to the project - Lilian
- Getting started with AWS API - Yann
- Learning of the technologies - Victor
## __Week 2 5th of February - 9th of February__ :

### __Monday 5th of February__ :
- Work on adding code quality in pipeline - Lilian
- Implementation of retrieval of AWS metrics - Yann
- Learning of the technologies - Victor

### __Tuesday 6th of February__ :
- Add code quality in pipeline - Lilian
- Do a first release of the project - Lilian
- Work on setup of database - Lilian & Yann
- Azure API documentation research - Victor

### __Wednesday 7th of February__ :
- Setup documentation of API - Lilian
- Work on adding multi-tenancy to the project - Lilian
- Work User connection - Yann
- Azure API documentation research - Victor

### __Thursday 8th of February__ :
- Work on adding multi-tenancy to the project - Lilian
- Work on user connection - Yann
- Azure API documentation research - Victor

### __Friday 9th of February__ :
- Work on adding multi-tenancy to the project - Lilian
- Work on AWS CRUD - Yann

## __Week 3 12th of February - 16th of February__ :
- Work on adding multi-tenancy to the project - Lilian
- Work on user connection - Yann

### __Monday 12th of February__ :
- Work on adding multi-tenancy to the project - Lilian
- Getting started with Grafana - Yann
- Work on getting Azure metrics - Victor
- Work on designing the specifications of our project for meeting with Mrs Trehoust

### __Tuesday 13th of February__ :
- Meeting with Mrs Trehoust - Lilian Yann Victor
- Work on adding multi-tenancy to the project - Lilian
- Work on adding RBAC (Role Based Access Control) to the project - Lilian
- Work on AWS CRUD - Yann
- Work on getting Azure metrics - Victor

### __Wednesday 14th of February__ :
- Work on adding multi-tenancy to the project - Lilian
- Work on adding RBAC (Role Based Access Control) to the project - Lilian
- Modified pipeline due to credit shortage - Lilian
- Work on AWS CRUD - Yann
- Work on getting Azure metrics - Victor

### __Thursday 15th of February__ :
- Work on adding multi-tenancy to the project - Lilian
- Work on adding RBAC (Role Based Access Control) to the project - Lilian
- Work on AWS CRUD - Yann
- Work on getting Azure metrics - Victor

### __Friday 16th of February__ :*
- Work on adding multi-tenancy to the project - Lilian
- Work on adding RBAC (Role Based Access Control) to the project - Lilian
- Getting started with Grafana - Yann
- Work on getting Azure metrics - Victor

## __Week 4 19th of February - 23th of February__ :

### __Monday 19th of February__ :
- Meeting with Chems-Eddine Hadiby
- Work on adding multi-tenancy to the project - Lilian
- Work on adding RBAC (Role Based Access Control) to the project - Lilian
- Work on Grafana BI with AWS - Yann
- Work on Azure CRUD - Victor

### __Tuesday 20th of February__ :
- MPI courses with Mrs Trehoust - Lilian & Yann & Victor
- Work on adding multi-tenancy to the project - Lilian
- Work on adding RBAC (Role Based Access Control) to the project - Lilian
- Made attemps on Grafana BI - Yann
- Work on Azure CRUD - Victor

### __Wednesday 21th of February__ :
- Work on requested changes from merge request - Lilian
- Review merge request for aws crud - Lilian
- Review merge request of multi-tenancy to the project - Yann
- Work on Azure CRUD - Victor

### __Thursday 22th of February__ :
- Review merge request for aws crud - Lilian
- Review merge request of multi-tenancy of the project - Yann & Victor
- Mid-term audit preparations - Lilian & Yann & Victor

### __Friday 23th of February__ :
- Work on adding heroku to the project - Lilian
- Review merge request for aws crud - Lilian
- Mid-term audit - Lilian & Victor & Yann

## __Week 5 26th of February - 1st of March__ :

### __Monday 26th of February__ :
- Work on adding heroku to the project - Lilian

## __Week 6 5th of March - 9th of March__ :

### __Monday 4th of March__ :
- Review merge request for azure crud - Lilian
- Creating english poster - Lilian & Victor
- Fixing suggestions on AWS CRUD merge request Yann
- Merge request Azure CRUD - Victor
- Review merge requests AWS + Heroku - Victor

### __Tuesday 5th of March__ :
- Review merge request for azure crud - Lilian
- Fixing suggestions on AWS CRUD merge request - Yann
- Fixing suggestions on Azure CRUD merge request - Victor

### __Wednesday 6th of March__ :
- MPI courses with Mrs Trehoust - Lilian & Yann & Victor
- English poster review with Mrs Gould - Lilian & Yann & Victor
- Review merge request for azure crud - Lilian
- Merging of AWS and multi-tenancy features - Yann
- Fixing suggestions on Azure CRUD merge request - Victor

### __Thursday 7th of March__ :
- Working on fixing deployment on server - Lilian
- Add documentation on AWS CRUD - Yann
- Fixing suggestions on Azure CRUD merge request - Victor

### __Friday 8th of March__ :
- Working on fixing deployment on server - Lilian
- Working on combined cost - Yann
- Working on improving Azure CRUD to help combined cost - Victor

## __Week 7 9th of March - 14th of March__ :

### __Monday 11th of March__ :
- Working on fixing deployment on server - Lilian
- Working on combined cost + attempts on Grafana - Yann
- Working on improving Azure CRUD to help combined cost - Victor
- Writing report for MPI - Lilian & Victor & Yann

### __Tuesday 12th of March__ :
- Working on fixing deployment on server - Lilian
- Merge request combined cost - Yann
- Review merge request - Victor

### __Wednesday 13th of March__ :
- Working on software metrics - Lilian
- Working on docs - Lilian & Yann & Victor
- English poster presentation - Lilian & Yann & Victor


